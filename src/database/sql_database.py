from math import isnan
from sqlalchemy import create_engine,MetaData,Table,Column,Integer,Float,Text
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql import select
from database.database_interface import DatabaseInterface
from pandas import DataFrame
from numpy import nan
from typing import List,Dict,Tuple
from common.constants import get_engine
from loguru import logger

def _get_database_types(column:str,column_dtype:str):
    
    dtype = None
    is_primary = False
    is_nullable = True
    if ('id'  == column.lower()):
        is_primary = True
        is_nullable = False
    if 'int' in column_dtype.lower():
        dtype = Integer
    elif 'float' in column_dtype.lower():
        dtype = Float
    elif 'object' or 'string' in column_dtype.lower():
        dtype = Text
    
    return Column(column, dtype, primary_key=is_primary, nullable=is_nullable)
    

class SqlDatabase(DatabaseInterface):
    def __init__(self) -> None:
        self.engine = create_engine(get_engine(),echo=True,future=True)
    
    

    def get_table(self,table_name,connection,dataframe:DataFrame)-> Table:
        """Receives a dataframe and a table name to return a Table. The table will be created if not exists

        Args:
            table_name (_type_): target table name
            connection (_type_): engine connection
            dataframe (DataFrame): data

        Returns:
            Table: table in sql database
        """

        logger.debug(f'Getting table {table_name}')
        metadata = MetaData(self.engine)
        table : Table = Table(table_name, metadata,
            *[_get_database_types(column,str(type(dataframe[column].dtype))) for column in dataframe.columns ])
        if not self.engine.dialect.has_table(connection, table_name):  # If table don't exist, Create.
            logger.debug(f"Table {table_name} doest exists. Creating it")
            metadata.create_all()
        else :
            logger.debug(f"Table {table_name} already exists.Returning table")
        logger.debug('Done!')
        return table
    #Responds diferently on each database    
    def select(self,table_name:str,dataframe:DataFrame,query=None) -> Tuple:
        """Do a select based on table name 

        Args:
            table_name (str): target table
            dataframe (DataFrame): data for check datatypes
            query (_type_, optional): query with where clauses. Defaults to None.

        Returns:
            Tuple: return of select statement
        """
        with self.engine.connect() as connection:
            table: Table = self.get_table(table_name,connection,dataframe)
            select = connection.execute(table.select(query))
            logger.error(select)
            return_values = []
            for values in select:
                return_values.append(values)
            return return_values
         
    
    def upsert(self, table_name: str,dataframe:DataFrame)-> bool:
        """Performs upsert in database receiving the table_name and the dataframe

        Args:
            table_name (str):
            dataframe (DataFrame): 

        Returns:
            bool: True if all works fine
        """
        logger.debug("Start upsert")
        
        
        values: List[Dict] = dataframe.to_dict('records')
        
     
        table:Table = None
        with self.engine.connect() as connection:

            table = self.get_table(table_name,connection,dataframe)
            
            for index,value in enumerate(values):
                for k,v in value.items():
                    #NaN is not allowed in 
                    if isinstance(v,float) and isnan(v):
                        values[index][k] = None
            
            for value in values:
                try:
                    logger.debug("Try insert")
                    connection.execute(table.insert(), value)
                    
                except IntegrityError as e:
                    logger.debug("Try update")
                    connection.execute(table.update(table.primary_key == value['id']),value)
                finally:
                    connection.commit()
                    
        logger.debug("Done upsert")
        return True