from fileinput import filename
from os import getenv,path
import pathlib





DOWNLOAD_PATH = getenv("DOWNLOAD_PATH",path.join(pathlib.Path(__file__).parent.parent.resolve(),"download"))
DOWNLOADED_FILENAME = path.join(DOWNLOAD_PATH,getenv("DOWNLOADED_FILENAME","Canada_Hosp1_COVID_InpatientData.xlsx.zip"))


def get_engine():
    return getenv("ENGINE_DB","mysql+pymysql://root@mysql:3306/covid?charset=utf8mb4")