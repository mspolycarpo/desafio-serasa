from loguru import logger
import zipfile
from os import path,makedirs
def unzip(zip_path:str,extraction_folder:str)-> None:
    """Receives the .zip file path and extract at extraction folder

    Args:
        zip_path (str): /path/to/zip/filename.zip
        extraction_folder (str): /path/to/extract/zip/
    """    
    logger.debug("Initializing unzipping")
    if not path.exists(extraction_folder):
        logger.debug(f"Extraction folder : {extraction_folder} doens't exist")
        logger.debug("Creating folder")
        makedirs(extraction_folder)
    with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        zip_ref.extractall(extraction_folder)
        logger.debug("Finalizing unzipping")
