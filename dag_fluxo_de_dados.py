"""This dag only runs some simple tasks to test Airflow's task execution."""
import datetime

import pendulum

from airflow.models.dag import DAG

from airflow.operators.dummy import DummyOperator
from airflow.operators.bash import BashOperator


now = pendulum.now(tz="UTC")
now_to_the_hour = (now - datetime.timedelta(0, 0, 0, 0, 0, 3)).replace(minute=0, second=0, microsecond=0)
START_DATE = now_to_the_hour
DAG_NAME = 'fluxo_de_dados'

dag = DAG(
    DAG_NAME,
    schedule_interval='*/10 * * * *',
    default_args={'depends_on_past': True},
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
)

start_task = DummyOperator(task_id='start', dag=dag)
finish_task = DummyOperator(task_id='finish', dag=dag)



from pathlib import Path
download_command = f" kaggle datasets download roche-data-science-coalition/uncover -f Canada_Hosp_COVID19_Inpatient_DatasetDefinitions/Canada_Hosp1_COVID_InpatientData.xlsx -p /opt/airflow/dags/src/download "
download_task = BashOperator(bash_command=download_command,task_id="download_task",dag=dag)
decompress_task = BashOperator(bash_command=f"python {Path(__file__).parent.resolve()}/src/decompress_data.py ",task_id="decompress_task",dag=dag)
transform_task = BashOperator(bash_command=f"python {Path(__file__).parent.resolve()}/src/transform_data.py ",task_id="transform_task",dag=dag)
database_task = BashOperator(bash_command=f"python {Path(__file__).parent.resolve()}/src/send_database.py ",task_id="database_task",dag=dag)


download_task.set_upstream(start_task)
decompress_task.set_upstream(download_task)
transform_task.set_upstream(decompress_task)
database_task.set_upstream(decompress_task)

finish_task.set_upstream([transform_task,database_task])