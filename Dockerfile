FROM apache/airflow:2.3.3-python3.10

USER root
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    vim \
    && apt-get autoremove -yqq --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
USER airflow



COPY --chown=airflow:root ./src /opt/airflow/dags/src

COPY --chown=airflow:root ./kaggle.json /opt/airflow/dags/src/kaggle.json

COPY ./.airflowignore /opt/airflow/dags/.airflowignore

COPY --chown=airflow:root dag_fluxo_de_dados.py /opt/airflow/dags

COPY ./requirements.txt /opt/airflow/dags/src/requirements.txt

RUN python -m pip install --upgrade pip

RUN  pip install -r  /opt/airflow/dags/src/requirements.txt

RUN mkdir /opt/airflow/dags/src/download

RUN airflow db init

RUN airflow users  create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin

ENV KAGGLE_CONFIG_DIR="/opt/airflow/dags/src/"

CMD ["airflow" ,"standalone"] 



EXPOSE 8080