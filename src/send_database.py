from loguru import logger
from database.database_interface import DatabaseInterface
from database.sql_database import SqlDatabase
from pandas import DataFrame,read_excel
from os import path
from common.constants import DOWNLOAD_PATH

def insert_or_update(db: DatabaseInterface,table:str,dataframe:DataFrame):
    """Performs upsert in a database

    Args:
        db (DatabaseInterface): database 
        table (str): table name
        dataframe (DataFrame): dataframe to insert/update

    Returns:
        _type_: True for if works fine
    """
    logger.info(f"Upsert into the table {table}")
    return db.upsert(table,dataframe)

def execute():
    df = read_excel(path.join(DOWNLOAD_PATH,'Canada_Hosp1_COVID_InpatientData.xlsx'),'Data-at-admission')
    db = SqlDatabase()
    
    df2 = read_excel(path.join(DOWNLOAD_PATH,'Canada_Hosp1_COVID_InpatientData.xlsx'),'Days-breakdown') 
    df3 = read_excel(path.join(DOWNLOAD_PATH,'Canada_Hosp1_COVID_InpatientData.xlsx'),'Hospital-length-of-stay') 
    df4 = read_excel(path.join(DOWNLOAD_PATH,'Canada_Hosp1_COVID_InpatientData.xlsx'),'Medication-Static-List') 


    insert_or_update(db,'admissions',df)
    insert_or_update(db,'breakdown',df2)
    insert_or_update(db,'stay',df3)
    insert_or_update(db,'medication',df4)
    
    
if __name__ == "__main__":
    execute()

    
    


    