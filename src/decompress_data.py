from common.utils import unzip
from common.constants import DOWNLOAD_PATH,DOWNLOADED_FILENAME


def execute():
    unzip(DOWNLOADED_FILENAME,DOWNLOAD_PATH)


if __name__ =="__main__":
    execute()