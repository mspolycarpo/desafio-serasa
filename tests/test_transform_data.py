from typing import Dict
from src.transform_data import top_values,get_group_percentage
from pandas import DataFrame
from random import randint
import pytest
def _make_random(top_number=10):
    return randint(0,top_number)
mock_data = {"ColumnA":[_make_random() for index in range(0,100)]}
mock_dataframe = DataFrame.from_dict(mock_data)
values = DataFrame.from_dict({"ColumnA":[1,2]})
mock_dataframe.add(values)

def test_top_values():
    distinct_values = 2
    dictionary = top_values(mock_dataframe,"ColumnA",distinct_values)
    assert isinstance(dictionary,Dict)
    assert len(dictionary.keys()) == distinct_values if distinct_values > 0 else 1
    
def test_top_values_less_than_one_distinct():
    dictionary = top_values(mock_dataframe,"ColumnA",0)
    assert len(dictionary.keys()) == 1

def test_top_values_fail_if_column_doest_exist():
    
    with pytest.raises(Exception):
        top_values(mock_dataframe,"Non_existent_column")

def test_get_group_percentage():
    keys = [1,2]
    dictionary = get_group_percentage(mock_dataframe,'ColumnA',keys)
    assert isinstance(dictionary,Dict)
    assert len(dictionary.keys()) == len(keys)
    
def test_get_group_percentage_not_found_key():
    keys = [11]
    dictionary = get_group_percentage(mock_dataframe,'ColumnA',keys)
    assert isinstance(dictionary,Dict)
    assert len(dictionary.keys()) == 0
    
def test_get_group_percentage_not_found_key_with_raise():
    keys = [11]
    with pytest.raises(Exception): 
        dictionary = get_group_percentage(mock_dataframe,'ColumnA',keys,throw_error=True)
   