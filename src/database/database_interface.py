from abc import ABC, abstractmethod
from typing import List,Dict
class DatabaseInterface(ABC):
    
    @abstractmethod
    def upsert(self):
        raise NotImplementedError("Method 'upsert' must be implemented")
    @abstractmethod
    def select(self):
        raise NotImplementedError("Method 'select' must be implemented")
 