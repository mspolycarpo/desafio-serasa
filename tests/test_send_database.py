

from unittest import mock
from os import environ
from pandas import DataFrame
from random import randint
from sqlalchemy.exc import OperationalError
from loguru import logger
from pytest import raises
from src.send_database import insert_or_update
from src.database.sql_database import SqlDatabase

def mockenv(**envvars):
    return mock.patch.dict(environ,envvars)

def _make_random(top_number=10):
    return randint(0,top_number)
mock_data = [] 
for index in range(0,2):
    mock_data.append({"id":index,"ColumnA":_make_random(1000)})
mock_dataframe = DataFrame.from_dict(mock_data)


@mockenv(ENGINE_DB="sqlite://")
def test_insert():
    db = SqlDatabase()
    is_upsert = insert_or_update(db,'test_insert_values',mock_dataframe)
    assert is_upsert == True
    
@mockenv(ENGINE_DB="sqlite://")
def test_update():
    db = SqlDatabase()
    insert_or_update(db,'test_update_values',mock_dataframe)
    is_upsert = insert_or_update(db,'test_update_values',mock_dataframe)
    
    assert is_upsert == True
 
@mockenv(ENGINE_DB="sqlite://")        
def test_upsert_fails_on_a_column_out_of_schema():
    db = SqlDatabase()
    diff_schema = mock_dataframe.copy()
    diff_schema['ColumnB'] = "STRING"
    insert_or_update(db,'test_update_values',mock_dataframe)
    logger.debug(diff_schema)
    with raises(OperationalError):
        is_upsert = insert_or_update(db,'test_update_values',diff_schema)