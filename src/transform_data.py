
import pandas as pd
from pandas import DataFrame
from common.constants import DOWNLOAD_PATH
from loguru import logger
from typing import Dict,List
from os import path



def top_values(dataframe: DataFrame,column_name:str ,distinct: int ) -> Dict:
    """_summary_

    Args:
        dataframe (DataFrame): any dataframe
        column_name (str): target column
        distinct (int): number of distinct top values 

    Raises:
        Exception: If column_name is not in dataframe

    Returns:
        Dict: dictionary with distinct top values
    """
    logger.info(f"Looking for top {distinct} values")
    if column_name not in dataframe.columns:
        raise Exception(f"The column name '{column_name}' is not in the dataframe provided")
    if distinct <= 0:
        logger.debug("'distinct' is less than 1. It will return only the top value")
        distinct = 1
    
    count = dataframe.value_counts(column_name)
    
    top_distinct = count[:distinct].to_dict()
    logger.info(f"Done looking for top values")

    return top_distinct




def get_group_percentage(dataframe:DataFrame,column_name:str,values:List,throw_error=False) -> Dict:
    """_summary_

    Args:
        dataframe (DataFrame): any dataframe
        column_name (str): target column
        values (List): list of values to get percentage
        throw_error (bool, optional): allow throw exception when a value in 'values' is not in dataframe . Defaults to False.

    Raises:
        Exception: Throws exception if the target column is not in dataframe provided
        Exception: Throws exception if a value in 'values' is not in the final dict

    Returns:
        Dict: _description_
    """
    logger.info(f"Get percent of values {values} from dataframe")   

    if column_name not in dataframe.columns:
        raise Exception(f"The column name '{column_name}' is not in the dataframe provided") 
    count = dataframe.value_counts(column_name)
    
    count_sum = count.sum()
    
    percentage_dict = {key:(value/count_sum)*100 for key,value in count.to_dict().items() } 
    
    filtered_dict = dict()
    message = "Value '{value}' is not a key in the dataframe"
    for value in values:
        if value in percentage_dict:
            filtered_dict[value] = percentage_dict[value]
        elif throw_error:
             raise Exception(message.format(value=value))
        else :
            logger.warning(message.format(value=value))
    

    
    logger.info("done!")   
    return filtered_dict


def execute():
    df = pd.read_excel(path.join(DOWNLOAD_PATH,'Canada_Hosp1_COVID_InpatientData.xlsx'),'Data-at-admission')
    distinct_reasons = 2
    dict_top_values : Dict = top_values(df,'reason_for_admission',distinct_reasons)
    logger.info(f"Top {distinct_reasons} reason(s) for admission:")
    for k,v in dict_top_values.items():
        logger.info(k)
        
    ages = [50,75,90]
    logger.info(f"Looking for percent of ages {ages}")
    dict_group_percentage :Dict = get_group_percentage(df,'age',ages)
    
    for k,v in dict_group_percentage.items():
        logger.info(f"Percent for {k} : {v}%")

if __name__=="__main__":
   execute()
        
    
    